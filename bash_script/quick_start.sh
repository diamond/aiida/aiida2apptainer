#!bin/bash
#
# Diamond project script to start a container

# Description function
usage()
{
	(
	echo "Usage: start_instance.sh [-a apptainer][-w workdir][-t timeout] container_image"
	echo
	echo "Start aiida instance from Diamond apptainer container."
	echo ""
	echo "-a apptainer absolute path to apptainer executable."
	echo "-t timeout   timeout."
	echo "-w workdir   absolute path to working directory."
	echo "-i image     name of the .sif apptainer image "
	echo ""
	) >&2
	exit 1
}

# Default values
if [ -z "${my_path}" ]; then my_path=~/aiida_dir ; fi
if [ -z "${apptainer}" ]; then apptainer=apptainer ; fi
if [ -z "${sif_image}" ]; then sif_image=aiida_numodis.sif_latest.sif ; fi
timeout=30

# script options and arguments
while true; do
  case "$1" in
    -a) apptainer="$2"; shift 2;;
    -t) timeout="$2"; shift 2;;
    -w) my_path="$2"; shift 2;;
    -*) usage;;
	*) break;;
  esac
done
mkdir -p $my_path

# function to wait for files to exist
function wait_for() {
    # check if the file exists every second
    until [ ${timeout_tmp} -le 0 ] || [ -e ${my_path}/.aiida/$@ ]; do
      sleep 1
      timeout_tmp=$(( timeout_tmp - 1 ))
    done

    if [ -e ${my_path}/.aiida/$@ ]; then
      # green OK
      echo -e "$@ \033[0;32mOK\033[0m"
    else
      # red timeout
      echo -e "$@ \033[0;31mtimeout\033[0m"
    fi
}

# create the necessary directories
cp ${sif_image} ${my_path}/.
cd $my_path
my_path=$(pwd)
mkdir -p .aiida/rabbitmq/var/lib/rabbitmq
mkdir -p .aiida/rabbitmq/var/log
mkdir -p .aiida/database .aiida/postgres_run .aiida/tmp
cp -r ~/.ssh .aiida/.

echo "Set up apptainer instance"
if [ $($apptainer instance list "aiida_instance" | wc -l) -gt 1 ]; then $apptainer instance stop aiida_instance; fi
$apptainer -s instance start --containall \
  -B .aiida:/.aiida \
  -B .aiida/tmp:/tmp \
  -B .aiida/.ssh:$HOME/.ssh \
  -B .aiida/postgres_run:/var/run/postgresql \
  -B .aiida/rabbitmq/var/lib/rabbitmq:/var/lib/rabbitmq \
  -B .aiida/rabbitmq/var/log:/var/log/rabbitmq \
  -B /volatile \
  ${sif_image} aiida_instance

# wait for postgresql and aiida to start
timeout_tmp=$timeout
wait_for init_postgresql_done
timeout_tmp=$timeout
wait_for init_aiida_done

# you're good to go, probably
echo -e "\033[1;33mInstance ready, you can now enter the container or launch a command using:\033[0m"
echo -e "    ${apptainer} shell instance://aiida_instance"
echo -e "    ${apptainer} exec instance://aiida_instance <command>"
