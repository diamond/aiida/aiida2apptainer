# We define some useful parameters
export TMP_PATH="/tmp"

# store flag instance start
sed -i 's@use=0@use=1@g' $TMP_PATH/state.env >> $TMP_PATH/state.env

### Start the PostgreSQL and RabbitMQ services
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";

# The database is initialized if it does not exist yet
if [ -s $AIIDA_PATH/database/PG_VERSION ]; then
	echo "Database already exists, skipping initialisation ..."
	echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
else
	/usr/lib/postgresql/15/bin/initdb $AIIDA_PATH/database
fi

# .... and start the server
/usr/lib/postgresql/15/bin/pg_ctl -D $AIIDA_PATH/database start

# we also start the RabbitMQ server
/usr/lib/rabbitmq/bin/rabbitmq-server -detached start