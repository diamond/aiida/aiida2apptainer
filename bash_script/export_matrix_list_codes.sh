export matrix_code="["
folder="apptainer/daughter_images"
for file in "$folder"/*; do
      file="${file%.def}"; file="${file#apptainer/daughter_images/}"
      matrix_code="${matrix_code} ${file},"
   done
matrix_code="${matrix_code%?} ]"