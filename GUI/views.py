# Import Tkinter module using 'tk' alias for a more Pythonic naming convention
import tkinter as tk
# Import the tkinter font module for working with different font styles
import tkinter.font as tkFont
import tkinter.scrolledtext as tkScrolledText

# Import 'yaml' module for parsing YAML files
import yaml
# Import 'subprocess' module for executing external programs
import subprocess

# Import all functions and classes from the 'utils' module
import utils
import view_utils


class Home():
    def __init__(self):
        self.window = tk.Tk()
        self.img_buttons = []
        self.n_column = 4

        view_utils.set_window_center(self.window, 
                                     1650, 900)
        view_utils.initialize_grid(self.window, 
                                   n_row=15, 
                                   n_column=self.n_column)
        self.window.title('Aiida for Diamond project')

        # Create poll based on config.json file ...
        i = 0
        n_row = 2

        main_page_dictionnary = utils.get_verdi_status()

        for key in main_page_dictionnary:

            # Using 'Label' widget to create Full name label and using place
            # method to set its position.
            lbl = tk.Label(self.window, 
                           text=key, font=(15),
                           anchor="e")
            view_utils.set_bold_font(lbl)
            lbl.grid(row=n_row+i, 
                     column=self.n_column//2-2, columnspan=2, 
                     sticky='E', 
                     padx=20)

            # Using Enrty widget to make a text entry box for accepting the
            # input string in text from user.
            lbl = tk.Label(self.window,
                           text=main_page_dictionnary[key], font=(15),
                           anchor="w")
            lbl.grid(row=n_row+i, 
                     column=self.n_column//2, columnspan=2, 
                     sticky='W', 
                     padx=20)

            # Increment vertical position
            i += 1

        # Place the diamond logo in the top right corner
        view_utils.place_diamond_logo(self)    

        # First time button
        txt = "First time? Let's set-up Aiida together!"
        btn = tk.Button(self.window,
                        text=txt, bg="black", fg='white', font=(15),
                        command=self.doFirstTimeUsingAiida)
        btn.grid(row=n_row+i+2, 
                 column=self.n_column//2-1, columnspan=2, 
                 sticky='NSEW', 
                 padx=15, pady=2)

        ###########
        # Easy mode
        txt = "Easy mode"
        lbl = tk.Label(self.window,
                       text=txt, font=(15))
        view_utils.set_bold_font(lbl)
        lbl.grid(row=n_row+i+3, 
                 column=self.n_column//2-1, 
                 sticky='NSEW', 
                 padx=20)

        # Show available computers button
        txt = "Show available computers"
        btn = tk.Button(self.window,
                        text=txt, bg="black", fg='white', font=(15),
                        command=self.doRender_available_computer)
        btn.grid(row=n_row+i+4, 
                 column=self.n_column//2-1, 
                 sticky='NSEW', 
                 padx=15, pady=2)

        # Show available codes button
        txt = "Show available codes"
        btn = tk.Button(self.window,
                        text=txt, bg="black", fg='white', font=(15),
                        command=self.doRender_available_code)
        btn.grid(row=n_row+i+5, 
                 column=self.n_column//2-1, 
                 sticky='NSEW', 
                 padx=15, pady=2)

        ###########
        # Advanced mode
        txt = "Advanced mode"
        lbl = tk.Label(self.window,
                       text=txt, font=(15))
        view_utils.set_bold_font(lbl)
        lbl.grid(row=n_row+i+3, 
                 column=self.n_column//2, 
                 sticky='NSEW', 
                 padx=20)

        # Show installed computers button
        txt = "Show installed computers"
        btn = tk.Button(self.window,
                        text=txt, bg="black", fg='white', font=(15),
                        command=self.doRender_installed_computer)
        btn.grid(row=n_row+i+4, 
                 column=self.n_column//2, 
                 sticky='NSEW', 
                 padx=15, pady=2)

        # Show installed codes button
        txt = "Show installed codes"
        btn = tk.Button(self.window,
                        text=txt, bg="black", fg='white', font=(15),
                        command=self.doRender_installed_code)
        btn.grid(row=n_row+i+5, 
                 column=self.n_column//2, 
                 sticky='NSEW', 
                 padx=15, pady=2)

    def doFirstTimeUsingAiida(self):
        self.window.destroy()
        page = Update_aiida_config(first_time=True)
        page.window.mainloop()

    def doRender_available_computer(self):
        self.window.destroy()
        page = Render_available_list("computer")
        page.window.mainloop()

    def doRender_available_code(self):
        self.window.destroy()
        page = Render_available_list("code")
        page.window.mainloop()

    def doRender_installed_computer(self):
        self.window.destroy()
        page = Render_installed_list("computer")
        page.window.mainloop()

    def doRender_installed_code(self):
        self.window.destroy()
        page = Render_installed_list("code")
        page.window.mainloop()


class Update_aiida_config():
    def __init__(self, first_time: bool = False):
        
        self.window = tk.Tk()
        self.img_buttons = []
        self.n_column = 7
        approx_rows = 15

        # Create the window at the center of the screen
        view_utils.set_window_center(self.window, 
                                     1650, 900)
        view_utils.initialize_grid(self.window, 
                                   n_row=approx_rows, 
                                   n_column=self.n_column)

        # Give a name to the current window
        self.window.title('Set-up your Aiida environment')

        # Give a title to the current window
        fontObj = tkFont.Font(size=20)
        title = tk.Label(self.window, 
                         text="Set-up your Aiida environment", font=fontObj)
        view_utils.set_bold_font(title)
        col, colspan = utils.get_col_colspan(self.n_column)
        title.grid(row=1, 
                   column=col, columnspan=colspan, 
                   sticky='NSEW', 
                   padx=20)

        i = 2
        self.lbl_list = []
        self.entry_list = []
        # Create poll update_aiida_config on config.json file ...
        with open('/tmp/config_description.yml', 'r') as data_file:
            description = yaml.safe_load(data_file)

        with open('/tmp/config.yml', 'r') as data_file:
            dictionnary = yaml.safe_load(data_file)

            for key in dictionnary:         

                # Create and place the current attribute 
                lbl = tk.Label(self.window,
                               text=description[key], font=(15),
                               anchor="e")
                view_utils.set_bold_font(lbl)
                lbl.grid(row=i+1, 
                         column=0, columnspan=2, 
                         sticky='E', 
                         padx=20)
                self.lbl_list.append(key)

                # Create and place the current entry that is the input string
                # from user
                enter = tk.Entry(self.window, font=(15))
                enter.insert(0, dictionnary[key])
                enter.grid(row=i+1, 
                           column=2, columnspan=self.n_column-2, 
                           sticky='WE', 
                           padx=20)
                self.entry_list.append(enter)

                # Increment vertical position
                i += 1

        # Place the home button in the top left corner
        view_utils.place_home_button(self) 

        # Place the diamond logo in the top right corner
        view_utils.place_diamond_logo(self)    

        # Create a button to execute the function update_config_yml
        txt = "Save changes"
        btn = tk.Button(self.window,
                        text=txt, bg="black", fg='white', font=(15),
                        command=lambda: 
                            utils.update_config_yml(self))
        btn.grid(row=i+2, 
                 column=self.n_column//2, 
                 sticky='NSEW', 
                 padx=15, pady=2)

        # Create a button to render list of all available computers if on first
        # use
        if first_time:
            txt = ">> Next step <<"
            btn = tk.Button(self.window,
                            text=txt, bg="black", fg='white', font=(15),
                            command=self.doRender_next_step_first_time)
            btn.grid(row=approx_rows-2, 
                     column=self.n_column-1, 
                     sticky='NSEW', 
                     padx=15, pady=2)

    def render_home(self):
        self.window.destroy()
        home = Home()
        home.window.mainloop()

    def doRender_next_step_first_time(self):
        self.window.destroy()
        page = Render_available_list("computer", first_time=True)
        page.window.mainloop()


class Render_available_list():
    def __init__(self, option: str, first_time: bool = False, 
                 first_time_last_step: bool = False):

        self.window = tk.Tk()
        self.img_buttons = []   
        self.given_list = []
        self.n_column = 31
        approx_rows = 15

        # Create the window at the center of the screen
        view_utils.set_window_center(self.window, 
                                     1650, 900)
        view_utils.initialize_grid(self.window, 
                                   n_row=approx_rows, 
                                   n_column=self.n_column)

        # Give a name to the current window
        self.window.title(f"List of {option}")

        # Give a title to the current window
        fontObj = tkFont.Font(size=20)
        txt = f"{option.capitalize()} available to install:"
        title = tk.Label(self.window, 
                         text=txt, font=fontObj)
        view_utils.set_bold_font(title)
        col, colspan = utils.get_col_colspan(self.n_column)
        title.grid(row=1, 
                   column=col+1, columnspan=colspan, 
                   sticky='NSEW', 
                   padx=20)

        # Create list of default config files ...
        if option == "computer":
            path = "/tmp/workspace/yml_files/computers/setup/"
        elif option == "code":
            path = "/tmp/workspace/yml_files/codes/"
        strings = subprocess.check_output(["ls", path])
        strings = strings.splitlines()
        i0 = 0

        for string in strings:
            string = str(string).replace("b'", "")
            string = string.replace("'", "")
            string = string.replace(".yml", "")

            # Using 'Label' widget to create Full name label and using place
            # method to set its position.
            lbl = tk.Label(self.window,
                           text=string, font=(15),
                           anchor="e")
            view_utils.set_bold_font(lbl)
            col_center = self.n_column//2 + 1
            lbl.grid(row=i0+3, 
                     column=col_center-col_center//2, columnspan=col_center//2, 
                     sticky='E', 
                     padx=10)
            self.given_list.append(string)

            view_utils.place_show_button(self, col_center + 1, i0, 
                                         f"available_{option}")
            view_utils.place_install_button(self, col_center + 2, i0, 
                                            f"{option}")

            # Increment vertical position
            i0 += 1

        # Place the home button in the top left corner
        view_utils.place_home_button(self) 

        # Place the diamond logo in the top right corner
        view_utils.place_diamond_logo(self)

        # Create a button to render list of all available computers if on first
        # use
        if first_time:
            txt = ">> Next step <<"
            btn = tk.Button(self.window,
                            text=txt, bg="black", fg='white', font=(15),
                            command=self.doRender_next_step_first_time)
            btn.grid(row=approx_rows-2, 
                     column=6*self.n_column//7+1, columnspan=3,
                     sticky='NSEW', 
                     padx=15, pady=2)  

        # Create a button to render list of all available computers if on first
        # use
        elif first_time_last_step:
            txt = ">> Return home <<"
            btn = tk.Button(self.window,
                            text=txt, bg="black", fg='white', font=(15),
                            command=self.render_home)
            btn.grid(row=approx_rows-2, 
                     column=6*self.n_column//7+1, columnspan=3,
                     sticky='NSEW', 
                     padx=15, pady=2)    

        else:
            # NB text ....
            txt = "Setup your Aiida environment before installing"
            lbl = tk.Label(self.window,
                           text=txt, font=(20),
                           anchor="e")
            view_utils.set_italic_font(lbl)
            lbl.grid(row=approx_rows-2, 
                     column=self.n_column//4+1, columnspan=self.n_column//2, 
                     sticky='E', 
                     padx=20)

            txt = ">> Click here <<"
            btn = tk.Button(self.window,
                            text=txt, bg="black", fg='white', font=(15),
                            command=self.doUpdate_aiida_config)
            btn.grid(row=approx_rows-2, 
                     column=3*self.n_column//4, columnspan=3,
                     sticky='NSEW', 
                     pady=2)

    def render_home(self):
        self.window.destroy()
        home = Home()
        home.window.mainloop()

    def doUpdate_aiida_config(self):
        self.window.destroy()
        page = Update_aiida_config()
        page.window.mainloop()

    def doRender_next_step_first_time(self):
        self.window.destroy()
        page = Render_available_list("code", first_time_last_step=True)
        page.window.mainloop()


class Render_installed_list():
    def __init__(self, option: str):

        self.window = tk.Tk()
        self.img_buttons = []   
        self.given_list = []
        self.n_column = 31
        approx_rows = 15

        # Create the window at the center of the screen
        view_utils.set_window_center(self.window, 
                                     1650, 900)
        view_utils.initialize_grid(self.window, 
                                   n_row=approx_rows, 
                                   n_column=self.n_column)

        # Give a name to the current window
        self.window.title(f"List of {option}")

        # Give a title to the current window
        fontObj = tkFont.Font(size=20)
        txt = f"{option.capitalize()} installed:"
        title = tk.Label(self.window, 
                         text=txt, font=fontObj)
        view_utils.set_bold_font(title)
        col, colspan = utils.get_col_colspan(self.n_column)
        title.grid(row=1, 
                   column=col+1, columnspan=colspan, 
                   sticky='NSEW', 
                   padx=20)

        # Create list of default config files ...
        self.given_list = utils.get_list_installed(option)
        i0 = 0

        for stuff in self.given_list:

            # Using 'Label' widget to create Full name label and using place
            # method to set its position.
            lbl = tk.Label(self.window,
                           text=stuff, font=(15),
                           anchor="e")
            view_utils.set_bold_font(lbl)
            col_center = self.n_column//2 + 1
            lbl.grid(row=i0+3, 
                     column=col_center-col_center//2, columnspan=col_center//2, 
                     sticky='E', 
                     padx=20)

            view_utils.place_show_button(self, col_center + 1, i0, 
                                         f"installed_{option}")
            view_utils.place_edit_button(self, col_center + 2, i0, 
                                         f"{option}")
            view_utils.place_delete_button(self, col_center + 3, i0, 
                                           f"{option}")

            # Increment vertical position
            i0 += 1

        # Place the home button in the top left corner
        view_utils.place_home_button(self) 

        # Place the diamond logo in the top right corner
        view_utils.place_diamond_logo(self)   

    def render_home(self):
        self.window.destroy()
        home = Home()
        home.window.mainloop()


class Show_additional_info():
    def __init__(self, i0: int, case: str, context):

        if "code" in case:
            option = "code"
        elif "computer" in case:
            option = "computer"

        if "installed" in case:
            dictionnary, num_lines = \
                utils.prepare_dictionnary_installed(i0, option, 
                                                    context.given_list)
        elif "available" in case:
            dictionnary, num_lines = \
                utils.prepare_dictionnary_available(i0, option, 
                                                    context.given_list)

        self.window = tk.Tk()
        self.img_buttons = []
        self.n_column = 13

        # Create the window at the center of the screen
        view_utils.set_window_center(self.window, 
                                     1500, 800)
        view_utils.initialize_grid(self.window, 
                                   n_row=sum(num_lines)+4, 
                                   n_column=self.n_column)

        # Give a name to the current window
        self.window.title(f"More info on {option}")

        # Give a title to the current window
        fontObj = tkFont.Font(size=20)
        txt = f"More info on {option}"
        title = tk.Label(self.window, 
                         text=txt, font=fontObj)
        view_utils.set_bold_font(title)
        col, colspan = utils.get_col_colspan(self.n_column)
        title.grid(row=0, 
                   column=col, columnspan=colspan, 
                   sticky='NSEW', 
                   padx=20)

        # Create list of default config files ...
        n = 2
        n_key = 0
        for key in dictionnary:

            # Using 'Label' widget to create Full name label and using place
            # method to set its position.
            lbl = tk.Label(self.window,
                           text=key, font=(15),
                           anchor="e")
            view_utils.set_bold_font(lbl)
            lbl.grid(row=n_key+n, 
                     column=0, columnspan=2, 
                     sticky='WE', 
                     padx=20)

            if num_lines[n_key] > 1:
                split_lines = dictionnary[key].split("\n")
                for line in split_lines:
                    
                    # Using Entry widget to make a text entry box for accepting
                    # the input string in text from user.
                    lbl = tk.Label(self.window,
                                   text=line, font=(15),
                                   anchor="w")
                    lbl.grid(row=n_key+n, 
                             column=2, columnspan=self.n_column-2, 
                             sticky='WE', 
                             padx=20)
                    n += 1
                n -= 1

            else:
                # Using Entry widget to make a text entry box for accepting the
                # input string in text from user.
                lbl = tk.Label(self.window,
                               text=dictionnary[key], font=(15),
                               anchor="w")
                lbl.grid(row=n_key+n, 
                         column=2, columnspan=self.n_column-2, 
                         sticky='WE', 
                         padx=20)

            n_key += 1    


class Edit_current_info():
    def __init__(self, i0: int, case: str, context):

        if "code" in case:
            option = "code"
        elif "computer" in case:
            option = "computer"

        dictionnary, num_lines = \
            utils.prepare_dictionnary_installed(i0, option, 
                                                context.given_list)

        self.window = tk.Tk()
        self.img_buttons = []
        self.n_column = 13
        self.max_rows = 6

        num_lines = [min(x, self.max_rows) for x in num_lines]

        # Create the window at the center of the screen
        view_utils.set_window_center(self.window, 
                                     1500, 800)
        view_utils.initialize_grid(self.window, 
                                   n_row=sum(num_lines)+3, 
                                   n_column=self.n_column)

        # Give a name to the current window
        txt = f"Edit configuration of {option}: {dictionnary['Label']}"
        self.window.title(txt)

        # Give a title to the current window
        fontObj = tkFont.Font(size=20)
        txt = f"Edit configuration of {option}: {dictionnary['Label']}"
        title = tk.Label(self.window, 
                         text=txt, font=fontObj)
        view_utils.set_bold_font(title)
        col, colspan = utils.get_col_colspan(self.n_column)
        title.grid(row=0, 
                   column=col, columnspan=colspan, 
                   sticky='NSEW', 
                   padx=20)

        # Create list of default config files ...
        n = 2
        n_key = 0
        self.lbl_list = []
        self.entry_list = []
        for key in dictionnary:

            # Using 'Label' widget to create Full name label and using place
            # method to set its position.
            lbl = tk.Label(self.window,
                           text=key, font=(15),
                           anchor="e")
            view_utils.set_bold_font(lbl)
            lbl.grid(row=n_key+n, 
                     column=0, columnspan=2, 
                     sticky='WE', 
                     padx=20)
            self.lbl_list.append(lbl)

            if num_lines[n_key] > 1:
                # Create and place the current entry that is the input
                # string from user

                enter = tkScrolledText.ScrolledText(self.window, wrap=tk.WORD, 
                                                    font=(15)) 
                enter.grid(row=n_key+n, rowspan=num_lines[n_key],
                           column=2, columnspan=self.n_column-3, 
                           sticky='WE', 
                           padx=20)
                enter.insert(tk.INSERT, dictionnary[key])
                enter.focus()
                self.entry_list.append(enter)

                n += num_lines[n_key]-1

            else:
                # Create and place the current entry that is the input string
                # from user
                enter = tk.Entry(self.window, font=(15))
                enter.insert(0, dictionnary[key])
                enter.grid(row=n_key+n, 
                           column=2, columnspan=self.n_column-3, 
                           sticky='WE', 
                           padx=20)
                self.entry_list.append(enter)

            n_key += 1    

        # Add button save changes
        txt = ">> Save changes <<"
        btn = tk.Button(self.window,
                        text=txt, bg="black", fg='white', font=(15),
                        height=1,
                        command=lambda: 
                            utils.update_current_config(self, 
                                                        num_lines, 
                                                        dictionnary['Label'], 
                                                        option))
        btn.grid(row=0, rowspan=2,
                 column=self.n_column-3, columnspan=2, 
                 padx=15, pady=15)


# pour créer nouvelle ligne
# Text = tk.Text(root, height=13, width=20, wrap=WORD)
