# Import Popup class, a utility for displaying information messages to users
from view_utils import Popup

# Import 'yaml' module for parsing YAML files
import yaml
# Import 'subprocess' module for executing external programs
import subprocess


def get_verdi_status():
    import aiida

    dictionnary = {
                "Version Aiida": None,
                "Path to config": None,
                "Profile name": None,
                "Postgresql status": None,
                "RabbitMQ status": None,
                "Aiida daemon status": None
            }

    profile = aiida.load_profile()
    manager = aiida.manage.manager.get_manager()
    settings = aiida.manage.configuration.settings

    dictionnary['Version Aiida'] = aiida.get_version().replace(".post0", "")
    dictionnary['Path to config'] = settings.AIIDA_CONFIG_FOLDER
    dictionnary['Profile name'] = profile.name

    try:
        storage_cls = profile.storage_cls
        _ = storage_cls.version_head()
        _ = storage_cls(profile)
        dictionnary['Postgresql status'] = \
            f"Storage for {dictionnary['Profile name']} opened!"
    except Exception:
        dictionnary['Postgresql status'] = \
            f"Storage for {dictionnary['Profile name']} closed!"
    
    try:
        manager.get_broker().get_communicator()
        dictionnary['RabbitMQ status'] = \
            f"Broker for {dictionnary['Profile name']} is running!"
    except Exception:
        dictionnary['RabbitMQ status'] = \
            f"Broker for {dictionnary['Profile name']} is not running!"

    try:
        dictionnary['Aiida daemon status'] = \
            manager.get_daemon_client().get_status()['status']
    except Exception:
        dictionnary['Aiida daemon status'] = "Absent"

    return dictionnary


def update_config_yml(context):
    # Calculate the length of the entry_list list
    n_entry = len(context.entry_list)
    
    # Create an empty dictionary to store entries
    dictionnary = {}
    
    # Iterate over all elements in entry_list and lbl_list
    for i in range(n_entry):
        # Use the label text as a key in the dictionary
        dictionnary[context.lbl_list[i]] = context.entry_list[i].get()
    
    # Write the dictionary data to a YAML file
    with open("/tmp/config.yml", "w") as outfile:
        yaml.dump(dictionnary, outfile, sort_keys=False)
    
    # Run a shell script to update the environment
    subprocess.run(["bash", "/tmp/config_env.sh"])
    
    # Display a popup confirming that information has been updated
    popup = Popup(context.window, [600, 100])
    txt = "Information updated in the config files!"
    popup.show_info_popup(context, txt)


def get_list_installed(option: str):
    import aiida

    installed_list = []

    if option == "code":
        code_collection = aiida.orm.Code.collection.all()

        for code in code_collection:
            installed_list.append(code.label)

    elif option == "computer":
        computer_collection = aiida.orm.Computer.collection.all()

        for computer in computer_collection:
            installed_list.append(computer.label)

    return installed_list


def update_current_config(context, num_lines, prev_cfg, option):
    import tkinter as tk

    if option == "code":

        # Create a dictionary to store entries
        dictionnary = {
                "label": "",
                "description": "",
                "default_calc_job_plugin": "",
                "filepath_executable": "",
                "computer": "",
                "prepend_text": "",
                "append_text": ""
            }

        # Iterate over all elements in entry_list and lbl_list
        n_entry = len(context.lbl_list)
        i0 = 0
        for i in range(n_entry):

            lbl = context.lbl_list[i].cget("text")
            lbl = lbl.lower()
            lbl = lbl.replace(" ", "_")

            # Use the label text as a key in the dictionary
            if lbl in dictionnary:
                if num_lines[i] > 1:
                    dictionnary[lbl] = context.entry_list[i0].get("1.0", 
                                                                  tk.END)
                else:
                    dictionnary[lbl] = context.entry_list[i0].get()
            i0 += 1

        # Write the dictionary data to a YAML file
        path2code = "/tmp/workspace/yml_files/codes/"
        with open(path2code + f"{dictionnary['label']}.yml", "w") as outfile:
            yaml.dump(dictionnary, outfile, sort_keys=False)

        # Delete the code feature using Verdi with input as 'yes' to confirm
        # deletion
        subprocess.check_output(["verdi", "code", "delete", f"{prev_cfg}"],
                                input="yes".encode())

        # Install the code feature using Verdi
        subprocess.check_output(["verdi", "code", "create",
                                 "core.code.installed", "-n", "--config", 
                                 path2code + f"{dictionnary['label']}.yml"])

        # Display a success message using the Popup class
        popup = Popup(context.window, [600, 100])
        txt = "Code successfully updated on your profile!"
        popup.show_info_popup(context, txt)

    elif option == "computer":

        # Create a dictionary to store entries
        dictionnary = {
                "label": "",
                "description": "",
                "hostname": "",
                "transport": "",
                "scheduler": "",
                "work_dir": "",
                "mpirun_command": "",
                "mpiprocs_per_machine": "",
                "prepend_text": ""
            }

        # Iterate over all elements in entry_list and lbl_list
        n_entry = len(context.lbl_list)
        i0 = 0
        for i in range(n_entry):

            lbl = context.lbl_list[i].cget("text")
            lbl = lbl.lower()
            lbl = lbl.replace(" ", "_")
            # Remove _type text from labels
            lbl = lbl.replace("_type", "")
            # Replace directory by dir
            lbl = lbl.replace("directory", "dir")
            # Replace default_#procs/machine by mpiprocs_per_machine
            lbl = lbl.replace("default_#procs/machine", "mpiprocs_per_machine")

            # Use the label text as a key in the dictionary
            if lbl in dictionnary:
                if num_lines[i] > 1:
                    dictionnary[lbl] = context.entry_list[i0].get("1.0", 
                                                                  tk.END)
                else:
                    dictionnary[lbl] = context.entry_list[i0].get()
            i0 += 1

        # Write the dictionary data to a YAML file
        path2setup = "/tmp/workspace/yml_files/computers/setup/"
        path2cfg = "/tmp/workspace/yml_files/computers/config/"
        with open(path2setup + f"{dictionnary['label']}.yml", "w") as outfile:
            yaml.dump(dictionnary, outfile, sort_keys=False)

        # Delete the code feature using Verdi with input as 'yes' to confirm
        # deletion
        subprocess.check_output(["verdi", "computer", "delete", 
                                 f"{prev_cfg}"], input="yes".encode())

        # Configure the computer feature using Verdi
        subprocess.check_output(["verdi", "computer", "setup", "-n", 
                                 "--config", 
                                 path2setup + f"{dictionnary['label']}.yml"])

        # Configure and install the computer feature using Verdi
        file = f"{dictionnary['transport'].replace('core.','')}"
        cfg_file = path2cfg + f"{file}.yml"
        subprocess.check_output(["verdi", "computer", "configure", 
                                 f"{dictionnary['transport']}", "-n", 
                                 "--config", cfg_file, 
                                 f"{dictionnary['label']}"])

        # Display a success message using the Popup class
        popup = Popup(context.window, [600, 100])
        txt = "Computer successfully updated on your profile!"
        popup.show_info_popup(context, txt)


def prepare_dictionnary_installed(i0: int, option: str, installed_list: list):
    # This function prepares a dictionary and a list of line counts for an
    # installed feature (either computer or code) based on the index and type
    # provided. The configuration for the installed feature is retrieved
    # using 'verdi' command-line tool.

    import aiida.orm

    if option == "code":
        code_collection = aiida.orm.Code.collection.all()
        code = [code for code in code_collection 
                if code.label == installed_list[i0]][0]

        dictionnary = {
            "Label": code.label,
            "Description": code.description,
            "Type": code.entry_point[0],
            "Default calc job plugin": code.default_calc_job_plugin,
            "Filepath executable": code.filepath_executable,
            "Computer": code.computer.label,
            "With mpi": code.with_mpi,
            "Prepend text": code.prepend_text,
            "Append text": code.append_text
        }

        if dictionnary["With mpi"] is None:
            dictionnary["With mpi"] = ""
        num_lines = [1] * len(dictionnary)
        num_lines[7] = dictionnary["Prepend text"].count("\n")
        num_lines[8] = dictionnary["Append text"].count("\n")

    elif option == "computer":
        computer_collection = aiida.orm.Computer.collection.all()
        computer = [computer for computer in computer_collection 
                    if computer.label == installed_list[i0]][0]

        mpiprocs_per_machine = computer.get_default_mpiprocs_per_machine()

        dictionnary = {
            "Label": computer.label,
            "Description": computer.description,
            "Hostname": computer.hostname,
            "Transport type": computer.transport_type,
            "Scheduler type": computer.scheduler_type,
            "Work directory": computer.get_workdir(),
            "Mpirun command": ' '.join(computer.get_mpirun_command()),
            "Default #procs/machine": mpiprocs_per_machine,
            "Prepend text": computer.get_prepend_text()
        }

        num_lines = [1] * len(dictionnary)
        num_lines[8] = dictionnary["Prepend text"].count("\n")

    return dictionnary, num_lines


def install_feature(i0, option: str, context):
    # This function installs a feature (either computer or code) based on the
    # index and type provided. The configuration for the feature is read from
    # a YAML file located at /tmp/workspace/yml_files.

    if option == "computer":
        # Install a computer type feature.
        computer = context.given_list[i0]
        path2setup = "/tmp/workspace/yml_files/computers/setup/"
        path2cfg = "/tmp/workspace/yml_files/computers/config/"
        with open(path2setup + f"{computer}.yml", 'r') as data_file:
            dictionnary = yaml.safe_load(data_file)

        # Configure the computer feature using Verdi
        subprocess.check_output(["verdi", "computer", "setup", "-n", 
                                 "--config", 
                                 path2setup + f"{computer}.yml"])

        # Configure and install the computer feature using Verdi
        file = f"{dictionnary['transport'].replace('core.','')}"
        cfg_file = path2cfg + f"{file}.yml"
        subprocess.check_output(["verdi", "computer", "configure", 
                                 f"{dictionnary['transport']}", "-n", 
                                 "--config", cfg_file, 
                                 f"{dictionnary['label']}"])

        # Display a success message using the Popup class
        popup = Popup(context.window, [600, 100])
        txt = f"Computer {dictionnary['label']} successfully " + \
              "installed on your profile!"
        popup.show_info_popup(context, txt)

    elif option == "code":
        # Install a code type feature.
        code = context.given_list[i0]
        path2code = "/tmp/workspace/yml_files/codes/"
        with open(path2code + f"{code}.yml", "r") as data_file:
            dictionnary = yaml.safe_load(data_file)

        # Install the code feature using Verdi
        subprocess.check_output(["verdi", "code", "create",
                                 "core.code.installed", "-n", "--config", 
                                 path2code + f"{code}.yml"])

        # Display a success message using the Popup class
        popup = Popup(context.window, [600, 100])
        txt = f"Code {dictionnary['label']} successfully " + \
              "installed on your profile!"
        popup.show_info_popup(context, txt)


def delete_feature(i0, option: str, context):
    # This function deletes a feature (either computer or code) based on the
    # index and type provided. The feature is deleted from the list of
    # installed features using Verdi command-line tool.

    if option == "computer":
        # Delete a computer type feature.
        computer = context.given_list[i0]

        # Delete the computer feature using Verdi with input as 'yes' to
        # confirm deletion
        subprocess.check_output(["verdi", "computer", "delete", f"{computer}"],
                                input="yes".encode())

        # Display a success message using the Popup class
        popup = Popup(context.window, [600, 100])
        txt = f"Computer {computer} successfully " + \
              "removed from your profile!"
        popup.show_info_popup(context, txt)

    elif option == "code":
        # Delete a code type feature.
        code = context.given_list[i0]

        # Delete the code feature using Verdi with input as 'yes' to confirm
        # deletion
        subprocess.check_output(["verdi", "code", "delete", f"{code}"], 
                                input="yes".encode())

        # Display a success message using the Popup class
        popup = Popup(context.window, [600, 100])
        txt = f"Code {code} successfully " + \
              "removed from your profile!"
        popup.show_info_popup(context, txt)


def prepare_dictionnary_available(i0: int, option: str, available_list: list):
    # This function prepares a dictionary and a list of line counts for a
    # feature (either computer or code) based on the index and type provided.
    # The configuration for the feature is read from a YAML file located
    # at /tmp/workspace/yml_files.
    if option == "computer":
        path = "/tmp/workspace/yml_files/computers/setup/"
    elif option == "code":
        path = "/tmp/workspace/yml_files/codes/"
    
    # Load the feature configuration from YAML file
    with open(path + available_list[i0] + '.yml', 'r') as data_file:
        dictionnary = yaml.safe_load(data_file)

    # Initialize a list of line counts with default value as 1
    num_lines = [1] * len(dictionnary)

    # Count the number of lines for each configuration property
    i = 0
    for key in dictionnary:
        num_lines[i] = len(dictionnary[key].split("\n"))  
        i += 1

    # Return the prepared dictionary and list of line counts
    return dictionnary, num_lines


def get_col_colspan(n_column):
    # We assume that n_column can be written as n_column=2n + 1

    n = n_column//2
    col = n//2
    colspan = n + 1
    if colspan % 2 == 0:
        colspan += 1

    return col, colspan
