# Import all functions and classes from the 'views' module
from views import Home
import os

# Create an instance of the 'Home' class defined in the 'views' module
# This line renders and displays the homepage of your application
os.chdir("/GUI")
home = Home()
home.window.mainloop()
