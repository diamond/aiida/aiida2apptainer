# Import the Tkinter module, which is used for creating graphical user
# interfaces (GUI) in Python
import tkinter as tk
# Import the tkinter font module for working with different font styles
import tkinter.font as tkFont

# Import 'ImageTk' and 'Image' modules from PIL (Pillow) library for working
# with images in Tkinter
from PIL import ImageTk, Image

import utils


class ToolTip():
    # This comes from an answer of squareroot17 on stackoverflow
    # https://stackoverflow.com/questions/20399243/display-message-when-hovering-over-something-with-mouse-cursor-in-python

    def __init__(self, widget):
        self.widget = widget
        self.tipwindow = None
        self.id = None
        self.x = self.y = 0

    def showtip(self, text):
        "Display text in tooltip window"
        self.text = text
        if self.tipwindow or not self.text:
            return
        x, y, cx, cy = self.widget.bbox("insert")
        x = x + self.widget.winfo_rootx() + 40
        y = y + cy + self.widget.winfo_rooty() + 15
        self.tipwindow = tw = tk.Toplevel(self.widget)
        tw.wm_overrideredirect(1)
        tw.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(tw, text=self.text, justify=tk.LEFT,
                         background="#ffffe0", relief=tk.SOLID, borderwidth=1,
                         font=("tahoma", "12", "normal"))
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tipwindow
        self.tipwindow = None
        if tw:
            tw.destroy()


def CreateToolTip(widget, text):
    toolTip = ToolTip(widget)

    def enter(event):
        toolTip.showtip(text)
    
    def leave(event):
        toolTip.hidetip()
    
    widget.bind('<Enter>', enter)
    widget.bind('<Leave>', leave)


class Popup():

    def __init__(self, window, size):
        self.window = window
        self.size = size
        self.fontObj = tkFont.Font(size=15)

    def show_info_popup(self, context, txt):
        # First, we create the popup window at the center of the screen ... 
        self.popup = tk.Toplevel(self.window)
        set_window_center(self.popup, self.size[0], self.size[1])
        initialize_grid(self.popup,
                        n_row=1, 
                        n_column=4)

        # ... we assign a title ...
        self.popup.title("Information popup")

        # ... we create the icon and store it in the current context ...
        img = Image.open(r"../assets/info.png").resize((45, 45))
        context.img_buttons.append(ImageTk.PhotoImage(img))

        # ... we display the icon on the left ...
        icon = tk.Label(self.popup,
                        image=context.img_buttons[-1], 
                        highlightthickness=0, bd=0)
        icon.grid(row=0, 
                  column=0, 
                  sticky='NSEW', 
                  padx=20, pady=20) 

        # ... and the text on the left
        lbl = tk.Label(self.popup, 
                       text=txt, font=self.fontObj,
                       wraplength=500,
                       anchor="w")
        lbl.grid(row=0,
                 column=1, columnspan=3,
                 sticky='NSEW',
                 pady=20)     


def set_bold_font(label):
    # Get the current font settings for the label and store them in a
    # dictionary
    font = tkFont.Font(font=label['font']).actual()
    
    # Set the label's font to be bold by updating its `font` attribute with new
    # values from the dictionary The new `font` value will have the same
    # family and size as before, but now with a 'bold' weight
    label.configure(font=(font['family'], font['size'], 'bold'))


def set_italic_font(label):
    # Get the current font settings for the label and store them in a
    # dictionary
    font = tkFont.Font(font=label['font']).actual()
    
    # Set the label's font to be italic by updating its `font` attribute with
    # new values from the dictionary The new `font` value will have the same
    # family and size as before, but now with a 'italic' weight
    label.configure(font=(font['family'], font['size'], 'italic'))


def set_window_center(window, width, height):
    # Get the screen width and height as integers
    w_s = window.winfo_screenwidth()
    h_s = window.winfo_screenheight()
    
    # Calculate the x and y coordinates of the top-left corner of the window
    # These values are set to be in the center of the screen, taking into
    # account the desired window width and height
    x_co = (w_s - width) // 2
    y_co = (h_s - height) // 2 + 50
    
    # Set the window's geometry to be the desired size and position This
    # specifies the window's dimensions and top-left corner coordinates in
    # pixels, using a string format: width x height + x-offset + y-offset
    window.geometry("%dx%d+%d+%d" % (width, height, x_co, y_co))
    
    # Set the minimum window size to be the same as the desired size
    window.minsize(width, height)


def initialize_grid(window, n_row, n_column):
    # Configure grid rows and columns for the given window
    
    # Loop through each row index (i.e., 0 to n_row-1)
    for i in range(n_row):
        # Configure the corresponding grid row for the window with a non-zero
        # weight and uniform "same_group" This means that any extra space
        # available when packing widgets into the grid will be distributed
        # evenly among rows with this setting
        window.grid_rowconfigure(index=i, weight=1, 
                                 uniform="same_group")
    
    # Loop through each column index (i.e., 0 to n_column-1)
    for j in range(n_column):
        # Configure the corresponding grid column for the window with a
        # non-zero weight and uniform "same_other_group" This means that any
        # extra space available when packing widgets into the grid will be
        # distributed evenly among columns with this setting
        window.grid_columnconfigure(index=j, weight=1, 
                                    uniform="same_other_group")


# def get_window_size(window):
#     # Return the current width and height of the given Tkinter window
    
#     # Call the "update_idletasks()" method to ensure that any pending layout or
#     # geometry changes have been applied to the window
#     window.update_idletasks()
    
#     # Use the "winfo_width()" and "winfo_height()" methods to retrieve the
#     # current width and height of the window, respectively
#     return window.winfo_width(), window.winfo_height()


def place_home_button(context):
    # Place a "Home" button on the Tkinter window using a given context object

    # Load and resize the home button image, then add it to the list of
    # available button images in the context object
    img = Image.open(r"../assets/home.png").resize((60, 60))
    context.img_buttons.append(ImageTk.PhotoImage(img))
    
    # Create a new Button widget for the "Home" button
    # The "command" property is set to the "render_home()" method of the
    # context object
    btn = tk.Button(context.window, 
                    image=context.img_buttons[-1], 
                    command=context.render_home, 
                    highlightthickness=0, bd=0)
    
    # Place the "Home" button in the top-left corner of the Tkinter window
    # using the grid geometry manager
    btn.grid(row=0, rowspan=3, 
             column=0, columnspan=3, 
             sticky='NW', 
             padx=20, pady=20)


def place_diamond_logo(context):
    # Place "Diamond Logo" on the Tkinter window using a given context object

    # Load and resize the Diamond Logo image, then add it to the list of
    # available button images in the context object
    img = Image.open(r"../assets/logo.png").resize((90, 90))
    context.img_buttons.append(ImageTk.PhotoImage(img))
    
    # Create a new Label widget for the "Diamond Logo" image
    lbl = tk.Label(context.window, 
                   image=context.img_buttons[-1], 
                   highlightthickness=0, bd=0)
    
    # Place the "Diamond Logo" image in the top-right corner of the Tkinter
    # window using the grid geometry manager
    lbl.grid(row=0, rowspan=3, 
             column=context.n_column, 
             sticky='NE', 
             padx=20, pady=20)


def place_show_button(context, j0, i0, case):
    # Place a "Show" button on the Tkinter window using a given context object
    from views import Show_additional_info

    # Load and resize the eye button image, then add it to the list of
    # available button images in the context object
    img = Image.open(r"../assets/eye.png").resize((30, 30))
    context.img_buttons.append(ImageTk.PhotoImage(img))
    
    # Create a new Button widget for the "Show" button
    # The "command" property is set to init the "Show_additional_info" class
    # with respect to the specified row, case, and `given_list` values from 
    # the context object as arguments
    btn = tk.Button(context.window, 
                    image=context.img_buttons[-1], 
                    command=lambda row=i0: 
                        Show_additional_info(i0, case, context), 
                    highlightthickness=0, bd=0)

    CreateToolTip(btn, text="Show this config")
    
    # Place the "Show" button in the Tkinter window using the grid geometry
    # manager
    btn.grid(row=i0 + 3, 
             column=j0, 
             sticky='W')


def place_install_button(context, j0, i0, case):
    # Place an "Install" button on the Tkinter window using the given context
    # object

    # Load and resize the download image, then add it to the list of available
    # button images in the context object
    img = Image.open(r"../assets/download.png").resize((30, 30))
    context.img_buttons.append(ImageTk.PhotoImage(img))
    
    # Create a new Button widget for the "Install" button
    # The "command" property calls the "install_feature" function
    # with respect to the specified row, case, and `given_list` values from 
    # the context object as arguments
    btn = tk.Button(context.window, 
                    image=context.img_buttons[-1], 
                    command=lambda row=i0: 
                        utils.install_feature(i0, case, context), 
                    highlightthickness=0, bd=0)

    CreateToolTip(btn, text="Install this config")

    # Place the "Install" button in the Tkinter window using the grid geometry
    # manager
    btn.grid(row=i0+3, 
             column=j0)


def place_edit_button(context, j0, i0, case):
    # Place a "Edit" button on the Tkinter window using a given context object
    from views import Edit_current_info

    # Load and resize the edit image, then add it to the list of available
    # button images in the context object
    img = Image.open(r"../assets/edit.png").resize((30, 30))
    context.img_buttons.append(ImageTk.PhotoImage(img))
    
    # Create a new Button widget for the "Edit" button
    # The "command" property is set to init the "Edit_current_info" class
    # with respect to the specified row, case, and `given_list` values from 
    # the context object as arguments
    btn = tk.Button(context.window, 
                    image=context.img_buttons[-1], 
                    command=lambda row=i0: 
                        Edit_current_info(i0, case, context), 
                    highlightthickness=0, bd=0)

    CreateToolTip(btn, text="Edit this config")

    # Place the "Edit" button in the Tkinter window using the grid geometry
    # manager
    btn.grid(row=i0+3, 
             column=j0)


def place_delete_button(context, j0, i0, case):
    # Place a "Delete" button on the Tkinter window using a given context
    # object

    # Load and resize the bin image, then add it to the list of available
    # button images in the context object
    img = Image.open(r"../assets/bin.png").resize((30, 30))
    context.img_buttons.append(ImageTk.PhotoImage(img))
    
    # Create a new Button widget for the "Delete" button
    # The "command" property calls the "delete_feature" function
    # with respect to the specified row, case, and `given_list` values from 
    # the context object as arguments
    btn = tk.Button(context.window, 
                    image=context.img_buttons[-1], 
                    command=lambda row=i0: 
                        utils.delete_feature(i0, case, context), 
                    highlightthickness=0, bd=0)

    CreateToolTip(btn, text="Delete this config")

    # Place the "Delete" button in the Tkinter window using the grid geometry
    # manager
    btn.grid(row=i0+3, 
             column=j0)
