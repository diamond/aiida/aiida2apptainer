[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/aiida/aiida2apptainer/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/aiida/aiida2apptainer/-/commits/main) 
[![coverage report](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/aiida/aiida2apptainer/badges/main/coverage.svg?min_good=90&min_acceptable=80&min_medium=60)](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/aiida/aiida2apptainer/-/commits/main) 


# Aiida2Apptainer

Repository with all produced Apptainer images of Aiida and documentation dedicated to workflows.


## Functionalities of the images

For now, here are the default functionalities of the different images:

- __computers__:
  - Dahu cluster from Gricad
  - Local computer
- __codes__:
  - VASP (to run on Dahu cluster)
- __ssh config__:
  - Config for Dahu cluster

## List of available images

For now, here are the daughter Aiida's apptainer images available on the [`Container Registry`](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/aiida/aiida2apptainer/container_registry):

- `aiida_vasp`
- `aiida_raspa`
- `aiida_raspa_eqeq`
- `aiida_numodis`
- `aiida_quantumespresso`

## Use an Aiida's Apptainer image

If you want to use an Aiida's Apptainer image, please read this [file](./tutorials/use_apptainer.md).

## Contributing

If you want to contribute to this projects, please read this [file](./CONTRIBUTING.md).
