# How to use Apptainer image of Aiida

## Step 0 : Before using the image

**If you have never used Apptainer before**, you must first install it on your local machine by following these [steps](https://diamond-diadem.github.io/en/documentation/install-apptainer/howto/) or check the documentation of the HPC cluster you want to use to see if it is available. It is recommended to use Apptainer **v1.2.X or higher**.

To use the Apptainer image of Aiida, you need a specific folder architecture. To make a long story short, we recommend that you use this folder architecture:

```bash
$ ls /path/to/folder/of/your/choice
└── .aiida (final container environment)
    ├── database
    ├── .ssh
    ├── postgres_run
    └── rabbitmq
         └── var
             ├── lib/rabbitmq
             └── log
```

Here is the long story:
* The `database` folder is needed to store the data related to Aiida.
* The `.ssh` folder is needed to store the data related to ssh connections.
* The `postgres_run` folder is needed to run the PostgreSQL service.
* The `rabbitmq/var/lib/rabbitmq` and `rabbitmq/var/log` folders are needed to run the RabbitMQ service.

Once you have created the folder architecture and you have installed Apptainer, you are ready to use any Apptainer image of Aiida.

## Step 1 : Get the image you need

The first thing you need to do in order to download any Apptainer image is to login to the gricad-gitlab OCI registry. The command to run depends on the version of Apptainer you are using.

### Apptainer v1.2.X

```bash
apptainer remote login --username {gitlab_username} oras://gricad-registry.univ-grenoble-alpes.fr
```

### Apptainer v1.3.X (and above)

```bash
apptainer registry login --username {gitlab_username} oras://gricad-registry.univ-grenoble-alpes.fr
```

### Download the image

The output of the above commands should be `INFO:    Token stored in $HOME/.apptainer/remote.yaml`. Then, you need to go to the `Deploy` section on the left panel of this repository, and in the `Container Registry` section. Select the image you are interested in (e.g., `aiida_vasp.sif`) and download it using the command:

```bash
apptainer pull oras://gricad-registry.univ-grenoble-alpes.fr/diamond/aiida/aiida2apptainer/aiida_vasp.sif:latest
```

## Step 2 : Launching an instance 

Once you have downloaded the image you want, it is time to use it! We recommend that you start an instance first, and then access the container using the `shell` command. To start an instance, you should run the following code:

```bash
apptainer instance start \
        --containall \
        -B $my_path/.aiida:/.aiida \
        -B $my_path/.aiida/.ssh:$HOME/.ssh \
        -B $my_path/.aiida/postgres_run:/var/run/postgresql \
        -B $my_path/.aiida/rabbitmq/var/lib/rabbitmq:/var/lib/rabbitmq \
        -B $my_path/.aiida/rabbitmq/var/log:/var/log/rabbitmq \
        {apptainer_image_name}.sif {instance_name}
```

Where, `$my_path` is an environment variable pointing to `/path/to/folder/of/your/choice` and the `-B` flags allow specific folders to be mounted on the Apptainer image. Once the instance has started, you can either access to the container or run commands inside it:

```bash
# to access the container
apptainer shell instance://{instance_name}

# to execute commands inside the container
apptainer exec instance://{instance_name} {commands}
```

If this is the **first time** you use this Apptainer image, you should configure your Aiida profile (follow [Step 3](#set-up-aiida)). On the other hand, you can use the image directly to run workflows (follow [Step 4](#run-workflow)).

## <a name=set-up-aiida></a>Step 3 : Setting up the Aiida environment

### Setting up a user profile

A default user profile is available in the container: `aiida_user`. However, you can create your own user profile using the following commands:

```bash
verdi quicksetup --profile {your_profile} --email {your@mail} --first-name {your_firstname} --last-name {your_lastname} --institution {your_institution}
verdi profile setdefault {your_profile}
verdi config set warnings.rabbitmq_version false
```

> The last command disables RabbitMQ warnings for your_profile.

### Setting up the aiida environment

Before configuring any computers or codes, we recommend that you set up your aiida environment. To do this, you can configure some environment variables in a `config.json` file using the following command:

```bash
nano-tiny /tmp/config.json
```

There is already some default values for the environment variables to help you out. Once you have personalised the file, you can run the command:

```bash
bash /tmp/config_env.sh
```

This will fully configure your aiida environment. This script will customise the default configuration files with the information you provide, making it easier to set up codes, computers and ssh connections.

### Setting up a seamless ssh connection to Gricad clusters from an Apptainer image

This section is intended for people who already have a PERSEUS account and are assigned to a project. If this is not your case, please check this [link](https://gricad-doc.univ-grenoble-alpes.fr/services/)

The first thing you need to do is to generate an RSA key. To do this, issue the following command: 
```bash
ssh-keygen
```

> It is very important to leave the password empty so that Aiida can access the Gricad facilities without any problems.

Then, you need to set up a transparent SSH connection. To do this, copy the `/tmp/workspace/ssh/config` file to `~/.ssh/`. Finally, you can add your RSA key to the head nodes and the clusters using the following commands:

```bash
ssh-copy-id {perseus-login}@rotule.univ-grenoble-alpes.fr
ssh-copy-id {perseus-login}@trinity.univ-grenoble-alpes.fr
ssh-copy-id dahu.ciment
```

You can try to access `dahu` cluster (with `ssh dahu.ciment`) to test if everything went well.

### <a name=activate-aiida></a>Activating the Aiida environment

To activate the Aiida environment, and have access to all the `verdi` commands, run the following command:

```bash
source /root/envs/aiida/bin/activate
```

### Setting up a computer

You can either take a look to some of the default computers in the `/tmp/workspace/yml_files/computers/setup` folder or write a custom `yml` file. To set up a new computer, you need to run the following commands:

```bash
verdi computer setup -n --config /tmp/workspace/yml_files/computers/setup/{computer}.yml
verdi computer configure {transport} -n --config /tmp/workspace/yml_files/computers/config/{computer}.yml {computer_name}
```

> Here, you need to change {computer}, {transport} and {computer_name} to the values you have chosen. The {transport} parameter is either `core.local` or `core.ssh` and it is written in the `/tmp/workspace/yml_files/computers/setup/{computer}.yml` file.

You can check that the installation went well with the `verdi computer list` command.

### Setting up a code

You can either have a look at some default codes in the folder `/tmp/workspace/yml_files/codes/` or write a custom `yml` file. To create a new code, you need to run the following command:

```bash
verdi code create core.code.installed -n --config /tmp/workspace/yml_files/codes/{code}.yml
```

You can check if the installation went well with the `verdi code list` command.

## <a name=run-workflow></a>Step 4 : Running a workflow

Once you have set up your environment correctly and [activated the aiida environment](#activate-aiida), you are ready to use the image and setup workflows. You can use either the `shell` or `exec` commands. We recommend that you use the `shell` command to access the container. We also recommend moving the workflow python file to `/tmp`.

To run a workflow, you just need to use the following command:

```bash
verdi run path/to/your/workflow/run.py
```

> **Caution!** Before running a workflow, you should look carefully at the python file and set up the correct {code_name} and {username}.

## Step 5 : Stopping an instance 

Once you have finished using the apptainer image, simply run the following command to stop the instance:

```bash
apptainer instance stop instance_name
```

## Additional steps

### Adding data to a code

Some codes and workflows require data. For example, VASP needs potentials to run correctly. In fact, you need to register data (as you did for the computer and the code). However, the way Aiida registers data is by providing a `.tar` or `.tar.gz` file and unpacking it. This means that you need to mount the `/tmp` folder on the container when you configure it. In practice, you need to add this line `-B /tmp:/tmp \` when starting the instance. Then, you can use the following command:

```bash
verdi data vasp-potcar uploadfamily --path=/tmp/{archive} --name=PBE.54 --description="PBE potentials version 54"
```

To check that the installation is correct, you can run `verdi data vasp-potcar listfamilies`. The output of this command should **not** be empty.

> Once you have added the data, we recommend that you to stop the instance and start a new one without the `/tmp` folder mounted on the container.

### Workflow on Dahu

You need to specify a Perseus project if you want to run workflows on the Dahu cluster from Gricad. To do this, you can write it as your username in your workflow python file.

## Known issues

Here is a list of all the known issues so far:

- No backward compatibility of Apptainer images from version **1.3.X** to version **1.2.X**. Building an image with apptainer **v1.3.X** will prevent it from being downloaded from the `gitlab-gricad` registry with apptainer **v1.2.X**.
- The postgresql service won't start is the 5432 port is already in use on that machine. In this case, it is impossible to create a profile for Aiida.
