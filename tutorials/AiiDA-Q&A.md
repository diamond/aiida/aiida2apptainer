# Common questions encountered using AiiDA

## Basic AiiDA use

### How to configure a computer/code

See Aiida documentation on [How to run external codes](https://aiida.readthedocs.io/projects/aiida-core/en/latest/howto/run_codes.html).

A computer or a code can be configured using the Command Line Interface (CLI) with a YAML file :
```
verdi computer setup --config config_file.yml
verdi code create core.code.installed --config config_file.yml
```
A configured code/computer can then be loaded with the python API:
```
computer = orm.load_computer('localhost')
code = orm.load_code('code_label@localhost')
```

Alternatively you can use the python API to setup a code:
```
code = orm.InstalledCode(label='code_label', computer=computer , filepath_executable='/absolute/path/to/executable.exe', default_calc_job_plugin='plugin_name')
```

### How to use a specific plugin 

Plugin help can be accessed using the Command Line Interface (CLI), for example:
```
# Display installed calculations plugins:
verdi plugin list aiida.calculations
# Display plugin help
verdi plugin list aiida.calculations numodis.raw
```

## Plugin developpment

### How to add labels/descriptions to workflow nodes

When using the `verdi node show #` command nodes display a label and a description which can be very useful to identify a specific step in the workflow. In the workchain the submit method returns a node which can be used to set this label and description, typically:
```
future = self.submit(numodisRaw, **inputs)
future.label="Dislocation Dynamics 0"
future.description=self.DislocationDynamics0.__doc__
```
Note that these changes require the package to be updated to be effective.

A default label/description can also be set for the workchain in the define method using:
```
spec.inputs['metadata']["label"].default="edgeloop2 workflow"
spec.inputs['metadata']["description"].default=edgeloop2WorkChain.__doc__
```

### How to add commands to the Command Line Interface (CLI)

It can be very useful to add commands for example to `verdi data` to use custom data classes. Examples of synthax can be found in the aiida source files in `aiida.cmdline.commands.cmd_data.cmd_singlefile`.

For these changes to be effective the corresponding entry-point must be added to the `pyproject.toml` file, typically:
```
[project.entry-points.'aiida.cmdline.data']
"numodis.xmlinput" = 'aiida_numodis.cli.data.xmlinput:cmd_xmlinput2'
```
To add commands outside `verdi` using the `click` python module, see the `aiida_quantumespresso` plugin for an example and update the `pyproject.toml` file with something similar to:
```
[project.scripts]
aiida-quantumespresso = 'aiida_quantumespresso.cli:cmd_root'
```