import pytest
import tkinter as tk
from views import Edit_current_info
from test_classes import Context


@pytest.mark.order(14)
def test_edit_current_info_computer_window():
    # List of computers available
    context = Context()
    context.given_list = [
        "localhost",
        "computer_example"
    ]
    i0 = 1
    option = "installed_computer"

    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Edit_current_info(i0, option, context).window

    print(window.winfo_children())
    
    # Save changes button
    button_save = window.winfo_children()[-1]
    assert button_save["text"] == ">> Save changes <<"
    assert isinstance(button_save, tk.Button)

    # Title of window
    label_window = window.winfo_children()[0]
    txt = "Edit configuration of computer: computer_example"
    assert label_window["text"] == txt
    assert isinstance(label_window, tk.Label)

    theo_dict = {
        "Label": 'computer_example',
        "Description": "yml file to install local computer on aiida",
        "Hostname": "localhost",
        "Transport type": "core.local",
        "Scheduler type": "core.direct",
        "Work directory": '/this/is/a/path',
        "Mpirun command": "mpirun -np {tot_num_mpiprocs}",
        "Default #procs/machine": '7',
        "Prepend text": ""
    }

    i0 = 1

    for key in theo_dict:
        # get label and entry pair
        label_name = window.winfo_children()[i0]
        label_value = window.winfo_children()[i0+1]
        assert label_name["text"] == key
        assert isinstance(label_name, tk.Label)
        assert label_value.get() == theo_dict[key]
        assert isinstance(label_value, tk.Entry)

        i0 += 2

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()


@pytest.mark.order(15)
def test_edit_current_info_code_window():
    # List of computers available
    context = Context()
    context.given_list = [
        "code_example"
    ]
    i0 = 0
    option = "installed_code"

    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Edit_current_info(i0, option, context).window

    print(window.winfo_children())
    
    # Save changes button
    button_save = window.winfo_children()[-1]
    assert button_save["text"] == ">> Save changes <<"
    assert isinstance(button_save, tk.Button)

    # Title of window
    label_window = window.winfo_children()[0]
    txt = "Edit configuration of code: code_example"
    assert label_window["text"] == txt
    assert isinstance(label_window, tk.Label)

    theo_dict = {
        "Label": 'code_example',
        "Description": "yml file to install VASP v6.3.2 on aiida",
        "Type": "core.code.installed",
        "Default calc job plugin": "vasp.vasp",
        "Filepath executable": "/this/is/another/path",
        "Computer": 'computer_example',
        "With mpi": "",
        "Prepend text": """source /applis/site/guix-start.sh
set -x
""",
        "Append text": ""
    }

    i0 = 1

    for key in theo_dict:
        # get label and entry pair
        label_name = window.winfo_children()[i0]
        assert label_name["text"] == key
        assert isinstance(label_name, tk.Label)
        i0 += 1

        label_value = window.winfo_children()[i0]
        lines = theo_dict[key].split("\n")
        if len(lines) == 1:
            assert label_value.get() == theo_dict[key]
            assert isinstance(label_value, tk.Entry)
        else:
            # assert label_value.get("1.0", tk.END) == theo_dict[key]
            assert isinstance(label_value, tk.Frame)
        i0 += 1

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()
