import pytest
import utils


@pytest.mark.order(16)
def test_get_col_colspan_pair():

    x = 8
    col, colspan = utils.get_col_colspan(x)

    assert col == 2
    assert colspan == 5


@pytest.mark.order(17)
def test_get_col_colspan_impair():

    x = 9
    col, colspan = utils.get_col_colspan(x)

    assert col == 2
    assert colspan == 5


@pytest.mark.order(18)
def test_prepare_dictionnary_available_computer():

    available_list = ["local_computer"]
    option = "computer"
    i0 = 0

    dictionnary, num_lines = \
        utils.prepare_dictionnary_available(i0, option, available_list)

    theo_dict = {
        "label": "$computer_name",
        "description": "yml file to install local computer on aiida",
        "hostname": "localhost",
        "transport": "core.local",
        "scheduler": "core.direct",
        "work_dir": "$local_workdir",
        "mpirun_command": "mpirun -np {tot_num_mpiprocs}",
        "mpiprocs_per_machine": "7",
        "prepend_text": " ",
    }

    theo_num_lines = [1] * len(theo_dict)

    assert theo_num_lines == num_lines
    assert theo_dict == dictionnary


@pytest.mark.order(19)
def test_prepare_dictionnary_available_code():

    available_list = ["vasp_gricad_dahu"]
    option = "code"
    i0 = 0

    dictionnary, num_lines = \
        utils.prepare_dictionnary_available(i0, option, available_list)

    theo_dict = {
        "label": "$code_name",
        "description": 'Custom VASP binary, executable defined by user',
        "default_calc_job_plugin": 'vasp.vasp',
        "filepath_executable": "$code_executable_path",
        "computer": "$computer_name",
        "prepend_text": """source /applis/site/guix-start.sh
set -x
cat $OAR_FILE_NODES | wc -l 

export OMPI_MCA_plm_rsh_agent=/usr/bin/oarsh
export OMP_NUM_THREADS=2
OPTS="-np 8 --machinefile $OAR_NODE_FILE \\
              --map-by ppr:2:socket:PE=4 --bind-to core \\
              -x OMP_NUM_THREADS=4 -x OMP_STACKSIZE=512m \\
              -x OMP_PLACES=cores -x OMP_PROC_BIND=close \\
              --report-bindings"

ENV="`guix time-machine -C $code_folder_path/channels.scm -- shell -m $code_folder_path/manifest.scm -- /bin/sh -c 'echo $GUIX_ENVIRONMENT'`"

exec ~/.config/guix/current/bin/guix shell -E ^OMPI -E ^OAR -E ^OMP \\
   -m $code_folder_path/manifest.scm \\
         -- mpirun $OPTS --prefix "$ENV" \\
""",
        "append_text": ' '
    }

    theo_num_lines = [1] * len(theo_dict)
    theo_num_lines[5] = len(theo_dict["prepend_text"].split("\n"))

    assert theo_num_lines == num_lines
    assert theo_dict == dictionnary


@pytest.mark.order(20)
def test_update_config_yml():
    import tkinter as tk
    import yaml
    from test_classes import Context

    context = Context()
    context.window = tk.Tk()

    # Create list of labels and entry
    context.lbl_list = [
        "perseus-login",
        "perseus-project",
        "computer_name",
        "code_name",
        "code_folder_path",
        "code_executable_path",
        "dahu_workdir",
        "local_workdir"
    ]

    values = [
        "arrondeb",
        "pr-test",
        "dahu_cluster",
        "code_vasp",
        "/home/arrondeb/vasp/6.3.2",
        "/home/arrondeb/vasp/6.3.2/bin/vasp_std",
        "/bettik/arrondeb/aiida_run",
        "/home/benjamin_arrondeau/aiida_run"
    ]

    for value in values:
        enter = tk.Entry(context.window)
        enter.insert(0, value)
        context.entry_list.append(enter)

    # update yml files
    utils.update_config_yml(context)

    # Load the updated configuration
    path = "/tmp/workspace/yml_files/computers/setup/"
    with open(path + 'gricad_dahu.yml', 'r') as data_file:
        comp_setup = yaml.safe_load(data_file)

    path = "/tmp/workspace/yml_files/codes/"
    with open(path + 'vasp_gricad_dahu.yml', 'r') as data_file:
        code = yaml.safe_load(data_file)

    # test updated fields for computer
    assert comp_setup["label"] == "dahu_cluster"
    assert comp_setup["work_dir"] == "/bettik/arrondeb/aiida_run"

    # test updated fields for code
    assert code["label"] == "code_vasp"
    assert code["filepath_executable"] == "/home/arrondeb/vasp/6.3.2/bin/vasp_std"
    assert code["computer"] == "dahu_cluster"

    context.window.destroy()


@pytest.mark.order(21)
def test_install_computer():
    import aiida.orm
    from test_classes import Context
    import tkinter as tk

    option = "computer"
    context = Context()
    context.window = tk.Tk()
    context.given_list = [
        "gricad_dahu",
        "local_computer"
    ]
    i0 = 1

    utils.install_feature(i0, option, context)

    computer_collection = aiida.orm.Computer.collection.all()

    assert len(computer_collection) == 3

    context.window.destroy()


@pytest.mark.order(22)
def test_install_code():
    import aiida.orm
    from test_classes import Context
    import tkinter as tk

    option = "code"
    context = Context()
    context.window = tk.Tk()
    context.given_list = [
        "vasp_gricad_dahu"
    ]
    i0 = 0

    utils.install_feature(i0, option, context)

    code_collection = aiida.orm.Code.collection.all()

    assert len(code_collection) == 2

    context.window.destroy()


@pytest.mark.order(23)
def test_delete_code():
    import aiida.orm
    from test_classes import Context
    import tkinter as tk

    option = "code"
    context = Context()
    context.window = tk.Tk()
    context.given_list = [
        "code_example",
        "code_vasp"
    ]
    i0 = 1

    utils.delete_feature(i0, option, context)

    code_collection = aiida.orm.Code.collection.all()

    assert len(code_collection) == 1

    context.window.destroy()


@pytest.mark.order(24)
def test_delete_computer():
    import aiida.orm
    from test_classes import Context
    import tkinter as tk

    option = "computer"
    context = Context()
    context.window = tk.Tk()
    context.given_list = [
        "localhost",
        "computer_example",
        "dahu_cluster"
    ]
    i0 = 2

    utils.delete_feature(i0, option, context)

    computer_collection = aiida.orm.Computer.collection.all()

    assert len(computer_collection) == 2

    context.window.destroy()


@pytest.mark.order(25)
def test_update_current_code():
    import tkinter as tk
    import tkinter.scrolledtext as tkScrolledText
    from test_classes import Context
    import aiida.orm
    from pathlib import Path

    # Create list of labels and entry
    theo_dict = {
        "Label": 'new_code_example',
        "Description": "New yml file to install VASP v6.3.2 on aiida",
        "Type": "core.code.installed",
        "Default calc job plugin": "vasp.vasp",
        "Filepath executable": "/this/is/new/path",
        "Computer": 'computer_example',
        "With mpi": "",
        "Prepend text": """source /applis/site/guix-start.sh
set -x
""",
        "Append text": ""
    }
    num_lines = [1, 1, 1, 1, 1, 1, 1, 2, 1]
    prev_cfg = "code_example"
    option = "code"

    context = Context()
    window = tk.Tk()
    i0 = 0
    for key in theo_dict:
        # First labels
        label = tk.Label(window, text=key)
        context.lbl_list.append(label)

        # Then entries
        if num_lines[i0] > 1:
            enter = tkScrolledText.ScrolledText(window) 
            enter.insert(tk.INSERT, theo_dict[key])
            context.entry_list.append(enter)
        else:
            enter = tk.Entry(window)
            enter.insert(0, theo_dict[key])
            context.entry_list.append(enter)
        i0 += 1

    # update yml files
    utils.update_current_config(context, num_lines, prev_cfg, option)

    # test updated fields for computer
    code_collection = aiida.orm.Code.collection.all()
    code = code_collection[-1]

    # test updated fields for code
    assert code.label == "new_code_example"
    assert code.description == "New yml file to install VASP v6.3.2 on aiida"
    assert code.filepath_executable == Path("/this/is/new/path")   

    window.destroy()


@pytest.mark.order(26)
def test_update_current_computer():
    import tkinter as tk
    from test_classes import Context
    import aiida.orm

    # Create list of labels and entry
    theo_dict = {
        "Label": 'new_computer_example',
        "Description": "New yml file to install local computer on aiida",
        "Hostname": "localhost",
        "Transport type": "core.local",
        "Scheduler type": "core.direct",
        "Work directory": '/this/is/a/new/path',
        "Mpirun command": "mpirun -np {tot_num_mpiprocs}",
        "Default #procs/machine": '16',
        "Prepend text": ""
    }
    num_lines = [1, 1, 1, 1, 1, 1, 1, 1, 1]
    prev_cfg = "computer_example"
    option = "computer"

    context = Context()
    window = tk.Tk()
    i0 = 0
    for key in theo_dict:
        # First labels
        label = tk.Label(window, text=key)
        context.lbl_list.append(label)

        # Then entries
        enter = tk.Entry(window)
        enter.insert(0, theo_dict[key])
        context.entry_list.append(enter)
        i0 += 1

    # update yml files
    utils.update_current_config(context, num_lines, prev_cfg, option)

    # test updated fields for computer
    computer_collection = aiida.orm.Computer.collection.all()
    computer = computer_collection[-1]

    # test updated fields for code
    assert computer.label == "new_computer_example"
    assert computer.description == "New yml file to install local computer on aiida"
    assert computer.get_workdir() == "/this/is/a/new/path"
    assert computer.get_default_mpiprocs_per_machine() == 16  

    window.destroy()
