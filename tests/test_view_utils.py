import pytest
import tkinter as tk
import view_utils


@pytest.mark.order(27)
def test_tool_tip():

    window = tk.Tk()
    txt = "This is an example text!"

    toolTip = view_utils.ToolTip(window)

    # First show the tip
    toolTip.showtip(txt)
    tipwindow = toolTip.tipwindow

    print(tipwindow.winfo_children())

    label = tipwindow.winfo_children()[0]
    assert label["text"] == txt
    assert isinstance(label, tk.Label)

    # Then destroy the tip
    toolTip.hidetip()
    assert toolTip.tipwindow is None


@pytest.mark.order(28)
def test_bold_font():

    window = tk.Tk()
    txt = "This is an example text!"
    label = tk.Label(window, text=txt)
    view_utils.set_bold_font(label)

    assert "bold" in label.cget("font")


@pytest.mark.order(29)
def test_italic_font():

    window = tk.Tk()
    txt = "This is an example text!"
    label = tk.Label(window, text=txt)
    view_utils.set_italic_font(label)

    assert "italic" in label.cget("font")


@pytest.mark.order(30)
def test_initialize_grid():

    window = tk.Tk()
    number_rows = 5
    number_columns = 10

    view_utils.initialize_grid(window, 
                               n_row=number_rows, 
                               n_column=number_columns)

    assert window.grid_size()[0] == number_columns
    assert window.grid_size()[1] == number_rows
