import aiida


def pytest_configure(config):
    """
    Allows plugins and conftest files to perform initial configuration.
    This hook is called for every plugin and initial conftest
    file after command line options have been parsed.
    """
    print("Configuration of testenv starting ...")
    # First we load aiida profile
    aiida.load_profile()

    print("Configuration of testenv finished!")
