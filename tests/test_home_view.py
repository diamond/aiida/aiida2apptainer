import pytest
import tkinter as tk
from views import Home


@pytest.mark.order(1)
def test_home_window():
    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Home().window
    
    # Advanced mode
    button_adv2 = window.winfo_children()[-1]
    button_adv1 = window.winfo_children()[-2]
    label_adv = window.winfo_children()[-3]
    assert button_adv2["text"] == "Show installed codes"
    assert button_adv1["text"] == "Show installed computers"
    assert label_adv["text"] == "Advanced mode"
    assert isinstance(button_adv2, tk.Button)
    assert isinstance(button_adv1, tk.Button)
    assert isinstance(label_adv, tk.Label)

    # Easy mode
    button_easy2 = window.winfo_children()[-4]
    button_easy1 = window.winfo_children()[-5]
    label_easy = window.winfo_children()[-6]
    assert button_easy2["text"] == "Show available codes"
    assert button_easy1["text"] == "Show available computers"
    assert label_easy["text"] == "Easy mode"
    assert isinstance(button_easy2, tk.Button)
    assert isinstance(button_easy1, tk.Button)
    assert isinstance(label_easy, tk.Label)

    # First time mode
    button_1st = window.winfo_children()[-7]
    assert button_1st["text"] == "First time? Let's set-up Aiida together!"
    assert isinstance(button_1st, tk.Button)

    # Diamond logo
    label_diamond = window.winfo_children()[-8]
    assert label_diamond["image"] != ""
    assert label_diamond["text"] == ""
    assert isinstance(label_diamond, tk.Label)

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()
