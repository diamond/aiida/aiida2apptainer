import pytest
import tkinter as tk
from views import Render_installed_list


@pytest.mark.order(8)
def test_render_installed_computer_window():
    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Render_installed_list(option="computer").window

    print(window.winfo_children())

    # Diamond logo
    label_diamond = window.winfo_children()[-1]
    assert label_diamond["image"] != ""
    assert label_diamond["text"] == ""
    assert isinstance(label_diamond, tk.Label)

    # Home button
    button_home = window.winfo_children()[-2]
    assert button_home["image"] != ""
    assert button_home["text"] == ""
    assert isinstance(button_home, tk.Button)

    # Title of window
    label_window = window.winfo_children()[0]
    assert label_window["text"] == "Computer installed:"
    assert isinstance(label_window, tk.Label)

    # List of computers available
    list_computer = [
        "localhost",
        "computer_example"
    ]

    i0 = 1

    for computer in list_computer:
        # get label and entry pair
        label = window.winfo_children()[i0]
        button_show = window.winfo_children()[i0+1]
        button_edit = window.winfo_children()[i0+2]
        button_delete = window.winfo_children()[i0+3]
        assert label["text"] == computer
        assert isinstance(label, tk.Label)
        assert button_show["text"] == ""
        assert button_show["image"] != ""
        assert isinstance(button_show, tk.Button)
        assert button_edit["text"] == ""
        assert button_edit["image"] != ""
        assert isinstance(button_edit, tk.Button)
        assert button_delete["text"] == ""
        assert button_delete["image"] != ""
        assert isinstance(button_delete, tk.Button)

        i0 += 4

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()


@pytest.mark.order(9)
def test_render_installed_code_window():
    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Render_installed_list(option="code").window

    print(window.winfo_children())

    # Diamond logo
    label_diamond = window.winfo_children()[-1]
    assert label_diamond["image"] != ""
    assert label_diamond["text"] == ""
    assert isinstance(label_diamond, tk.Label)

    # Home button
    button_home = window.winfo_children()[-2]
    assert button_home["image"] != ""
    assert button_home["text"] == ""
    assert isinstance(button_home, tk.Button)

    # Title of window
    label_window = window.winfo_children()[0]
    assert label_window["text"] == "Code installed:"
    assert isinstance(label_window, tk.Label)

    # List of computers available
    list_computer = [
        "code_example"
    ]

    i0 = 1

    for computer in list_computer:
        # get label and entry pair
        label = window.winfo_children()[i0]
        button_show = window.winfo_children()[i0+1]
        button_edit = window.winfo_children()[i0+2]
        button_delete = window.winfo_children()[i0+3]
        assert label["text"] == computer
        assert isinstance(label, tk.Label)
        assert button_show["text"] == ""
        assert button_show["image"] != ""
        assert isinstance(button_show, tk.Button)
        assert button_edit["text"] == ""
        assert button_edit["image"] != ""
        assert isinstance(button_edit, tk.Button)
        assert button_delete["text"] == ""
        assert button_delete["image"] != ""
        assert isinstance(button_delete, tk.Button)

        i0 += 4

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()
