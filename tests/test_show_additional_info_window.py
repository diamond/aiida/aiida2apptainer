import pytest
import tkinter as tk
from views import Show_additional_info
from test_classes import Context


@pytest.mark.order(10)
def test_show_additional_info_installed_computer_window():

    # List of computers available
    context = Context()
    context.given_list = [
        "localhost",
        "computer_example"
    ]
    i0 = 1
    option = "installed_computer"

    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Show_additional_info(i0, option, context).window

    print(window.winfo_children())

    # Title of window
    label_window = window.winfo_children()[0]
    assert label_window["text"] == "More info on computer"
    assert isinstance(label_window, tk.Label)

    theo_dict = {
        "Label": 'computer_example',
        "Description": "yml file to install local computer on aiida",
        "Hostname": "localhost",
        "Transport type": "core.local",
        "Scheduler type": "core.direct",
        "Work directory": '/this/is/a/path',
        "Mpirun command": "mpirun -np {tot_num_mpiprocs}",
        "Default #procs/machine": 7,
        "Prepend text": ""
    }

    i0 = 1

    for key in theo_dict:
        # get label and entry pair
        label_name = window.winfo_children()[i0]
        label_value = window.winfo_children()[i0+1]
        assert label_name["text"] == key
        assert isinstance(label_name, tk.Label)
        assert label_value["text"] == theo_dict[key]
        assert isinstance(label_value, tk.Label)

        i0 += 2

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()


@pytest.mark.order(11)
def test_show_additional_info_installed_code_window():

    # List of computers available
    context = Context()
    context.given_list = [
        "code_example"
    ]
    i0 = 0
    option = "installed_code"

    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Show_additional_info(i0, option, context).window

    print(window.winfo_children())

    # Title of window
    label_window = window.winfo_children()[0]
    assert label_window["text"] == "More info on code"
    assert isinstance(label_window, tk.Label)

    theo_dict = {
        "Label": 'code_example',
        "Description": "yml file to install VASP v6.3.2 on aiida",
        "Type": "core.code.installed",
        "Default calc job plugin": "vasp.vasp",
        "Filepath executable": "/this/is/another/path",
        "Computer": 'computer_example',
        "With mpi": "",
        "Prepend text": """source /applis/site/guix-start.sh
set -x
""",
        "Append text": ""
    }

    i0 = 1

    for key in theo_dict:
        # get label and entry pair
        label_name = window.winfo_children()[i0]
        assert label_name["text"] == key
        assert isinstance(label_name, tk.Label)
        i0 += 1

        for line in theo_dict[key].split("\n"):
            label_value = window.winfo_children()[i0]
            assert label_value["text"] == line
            assert isinstance(label_value, tk.Label)
            i0 += 1

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()


@pytest.mark.order(12)
def test_show_additional_info_available_computer_window():

    # List of computers available
    context = Context()
    context.given_list = [
        "gricad_dahu",
        "local_computer"
    ]
    i0 = 1
    option = "available_computer"

    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Show_additional_info(i0, option, context).window

    print(window.winfo_children())

    # Title of window
    label_window = window.winfo_children()[0]
    assert label_window["text"] == "More info on computer"
    assert isinstance(label_window, tk.Label)

    theo_dict = {
        "label": '$computer_name',
        "description": "yml file to install local computer on aiida",
        "hostname": "localhost",
        "transport": "core.local",
        "scheduler": "core.direct",
        "work_dir": '$local_workdir',
        "mpirun_command": "mpirun -np {tot_num_mpiprocs}",
        "mpiprocs_per_machine": '7',
        "prepend_text": " "
    }

    i0 = 1

    for key in theo_dict:
        # get label and entry pair
        label_name = window.winfo_children()[i0]
        label_value = window.winfo_children()[i0+1]
        print(label_name["text"], label_value["text"])
        assert label_name["text"] == key
        assert isinstance(label_name, tk.Label)
        assert label_value["text"] == theo_dict[key]
        assert isinstance(label_value, tk.Label)

        i0 += 2

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()


@pytest.mark.order(13)
def test_show_additional_info_available_code_window():

    # List of computers available
    context = Context()
    context.given_list = [
        "vasp_gricad_dahu"
    ]
    i0 = 0
    option = "available_code"

    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Show_additional_info(i0, option, context).window

    print(window.winfo_children())

    # Title of window
    label_window = window.winfo_children()[0]
    assert label_window["text"] == "More info on code"
    assert isinstance(label_window, tk.Label)

    theo_dict = {
        "label": '$code_name',
        "description": "Custom VASP binary, executable defined by user",
        "default_calc_job_plugin": "vasp.vasp",
        "filepath_executable": "$code_executable_path",
        "computer": '$computer_name',
        "prepend_text": """source /applis/site/guix-start.sh
set -x
cat $OAR_FILE_NODES | wc -l 

export OMPI_MCA_plm_rsh_agent=/usr/bin/oarsh
export OMP_NUM_THREADS=2
OPTS="-np 8 --machinefile $OAR_NODE_FILE \\
              --map-by ppr:2:socket:PE=4 --bind-to core \\
              -x OMP_NUM_THREADS=4 -x OMP_STACKSIZE=512m \\
              -x OMP_PLACES=cores -x OMP_PROC_BIND=close \\
              --report-bindings"

ENV="`guix time-machine -C $code_folder_path/channels.scm -- shell -m $code_folder_path/manifest.scm -- /bin/sh -c 'echo $GUIX_ENVIRONMENT'`"

exec ~/.config/guix/current/bin/guix shell -E ^OMPI -E ^OAR -E ^OMP \\
   -m $code_folder_path/manifest.scm \\
         -- mpirun $OPTS --prefix "$ENV" \\
""",
        "append_text": " "
    }

    i0 = 1

    for key in theo_dict:
        # get label and entry pair
        label_name = window.winfo_children()[i0]
        assert label_name["text"] == key
        assert isinstance(label_name, tk.Label)
        i0 += 1

        for line in theo_dict[key].split("\n"):
            label_value = window.winfo_children()[i0]
            assert label_value["text"] == line
            assert isinstance(label_value, tk.Label)
            i0 += 1

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()
