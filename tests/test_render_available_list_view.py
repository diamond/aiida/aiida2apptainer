import pytest
import tkinter as tk
from views import Render_available_list


@pytest.mark.order(4)
def test_render_available_computer_window():
    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Render_available_list(option="computer").window

    print(window.winfo_children())
    
    # Go to Aiida setup button
    button_gotosetup = window.winfo_children()[-1]
    label_gotosetup = window.winfo_children()[-2]
    assert button_gotosetup["text"] == ">> Click here <<"
    assert isinstance(button_gotosetup, tk.Button)
    txt = "Setup your Aiida environment before installing"
    assert label_gotosetup["text"] == txt
    assert isinstance(label_gotosetup, tk.Label)

    # Diamond logo
    label_diamond = window.winfo_children()[-3]
    assert label_diamond["image"] != ""
    assert label_diamond["text"] == ""
    assert isinstance(label_diamond, tk.Label)

    # Home button
    button_home = window.winfo_children()[-4]
    assert button_home["image"] != ""
    assert button_home["text"] == ""
    assert isinstance(button_home, tk.Button)

    # Title of window
    label_window = window.winfo_children()[0]
    assert label_window["text"] == "Computer available to install:"
    assert isinstance(label_window, tk.Label)

    # List of computers available
    list_computer = [
        "gricad_dahu",
        "local_computer"
    ]

    i0 = 1

    for computer in list_computer:
        # get label and entry pair
        label = window.winfo_children()[i0]
        button_show = window.winfo_children()[i0+1]
        button_install = window.winfo_children()[i0+2]
        assert label["text"] == computer
        assert isinstance(label, tk.Label)
        assert button_show["text"] == ""
        assert button_show["image"] != ""
        assert isinstance(button_show, tk.Button)
        assert button_install["text"] == ""
        assert button_install["image"] != ""
        assert isinstance(button_install, tk.Button)

        i0 += 3

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()


@pytest.mark.order(5)
def test_render_available_code_window():
    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Render_available_list(option="code").window

    print(window.winfo_children())
    
    # Go to Aiida setup button
    button_gotosetup = window.winfo_children()[-1]
    label_gotosetup = window.winfo_children()[-2]
    assert button_gotosetup["text"] == ">> Click here <<"
    assert isinstance(button_gotosetup, tk.Button)
    txt = "Setup your Aiida environment before installing"
    assert label_gotosetup["text"] == txt
    assert isinstance(label_gotosetup, tk.Label)

    # Diamond logo
    label_diamond = window.winfo_children()[-3]
    assert label_diamond["image"] != ""
    assert label_diamond["text"] == ""
    assert isinstance(label_diamond, tk.Label)

    # Home button
    button_home = window.winfo_children()[-4]
    assert button_home["image"] != ""
    assert button_home["text"] == ""
    assert isinstance(button_home, tk.Button)

    # Title of window
    label_window = window.winfo_children()[0]
    assert label_window["text"] == "Code available to install:"
    assert isinstance(label_window, tk.Label)

    # List of computers available
    list_computer = [
        "vasp-gam_gricad_dahu",
        "vasp-std_gricad_dahu",
        "vasp_gricad_dahu"
    ]

    i0 = 1

    for computer in list_computer:
        # get label and entry pair
        label = window.winfo_children()[i0]
        button_show = window.winfo_children()[i0+1]
        button_install = window.winfo_children()[i0+2]
        assert label["text"] == computer
        assert isinstance(label, tk.Label)
        assert button_show["text"] == ""
        assert button_show["image"] != ""
        assert isinstance(button_show, tk.Button)
        assert button_install["text"] == ""
        assert button_install["image"] != ""
        assert isinstance(button_install, tk.Button)

        i0 += 3

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()


@pytest.mark.order(6)
def test_render_available_window_first_time():
    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Render_available_list(first_time=True, option="computer").window

    print(window.winfo_children())
    
    # Next step button
    button_next = window.winfo_children()[-1]
    assert button_next["text"] == ">> Next step <<"
    assert isinstance(button_next, tk.Button)

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()


@pytest.mark.order(7)
def test_render_available_window_first_time_last_step():
    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Render_available_list(first_time_last_step=True, option="computer").window

    print(window.winfo_children())
    
    # Next step button
    button_next = window.winfo_children()[-1]
    assert button_next["text"] == ">> Return home <<"
    assert isinstance(button_next, tk.Button)

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()
