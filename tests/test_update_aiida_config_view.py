import pytest
import tkinter as tk
from views import Update_aiida_config


@pytest.mark.order(2)
def test_update_aiida_config_window():
    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Update_aiida_config().window

    print(window.winfo_children())
    
    # Save changes button
    button_save = window.winfo_children()[-1]
    assert button_save["text"] == "Save changes"
    assert isinstance(button_save, tk.Button)

    # Diamond logo
    label_diamond = window.winfo_children()[-2]
    assert label_diamond["image"] != ""
    assert label_diamond["text"] == ""
    assert isinstance(label_diamond, tk.Label)

    # Home button
    button_home = window.winfo_children()[-3]
    assert button_home["image"] != ""
    assert button_home["text"] == ""
    assert isinstance(button_home, tk.Button)

    # Title of window
    label_window = window.winfo_children()[0]
    assert label_window["text"] == "Set-up your Aiida environment"
    assert isinstance(label_window, tk.Label)

    # Env. variable
    theo_description = {
        "perseus-login": "Login of your perseus account",
        "perseus-project": "Project assigned to your perseus account",
        "computer_name": "Label used by Aiida for the computer",
        "code_name": "Label used by Aiida for the code",
        "code_folder_path": "Path to the code folder",
        "code_executable_path": "Path to the code executable",
        "dahu_workdir": "Path to the work directory on Dahu",
        "local_workdir": "Path to the work directory on your local machine"
    }

    theo_config = {
        "perseus-login": "arrondeb",
        "perseus-project": "pr-test",
        "computer_name": "dahu_cluster",
        "code_name": "code_vasp",
        "code_folder_path": "/home/arrondeb/vasp/6.3.2",
        "code_executable_path": "/home/arrondeb/vasp/6.3.2/bin/vasp_std",
        "dahu_workdir": "/bettik/arrondeb/aiida_run",
        "local_workdir": "/home/benjamin_arrondeau/aiida_run"
    }

    i0 = 1

    for key in theo_description:
        # get label and entry pair
        label = window.winfo_children()[i0]
        entry = window.winfo_children()[i0+1]
        assert label["text"] == theo_description[key]
        assert isinstance(label, tk.Label)
        assert entry.get() == theo_config[key]
        assert isinstance(entry, tk.Entry)

        i0 += 2

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()


@pytest.mark.order(3)
def test_update_aiida_config_window_first_time():
    # Crée une nouvelle fenêtre et des références pour les widgets
    window = Update_aiida_config(first_time=True).window

    print(window.winfo_children())
    
    # Next step button
    button_next = window.winfo_children()[-1]
    assert button_next["text"] == ">> Next step <<"
    assert isinstance(button_next, tk.Button)

    # Vérifie que la fenêtre et les widgets ont bien été créés
    assert isinstance(window, tk.Tk)

    # Ferme la fenêtre pour éviter les warnings de Pytest
    window.destroy()
